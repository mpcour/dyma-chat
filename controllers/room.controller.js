const {
  getRooms,
  createRoom,
  updateRoom,
  deleteRoom,
} = require("../queries/room.queries");
const {
  deleteMessagesPerRoomId,
  findMessagesPerRoomId,
} = require("../queries/message.queries");

const tryGetRoomsController = async (socketOrServer) => {
  try {
    const rooms = await getRooms();
    socketOrServer.emit("rooms", rooms);
  } catch (e) {
    throw e;
  }
};

const createRoomController = async ({ room, ios }) => {
  try {
    await createRoom(room);
    tryGetRoomsController(ios);
  } catch (e) {
    throw e;
  }
};

const updateRoomController = async ({ room, ios }) => {
  try {
    if (room.title) {
      await updateRoom(room);
      const rooms = await getRooms();
      ios.emit("rooms", rooms);
    }
  } catch (e) {
    throw e;
  }
};

const deleteRoomController = async ({ room, ios }) => {
  try {
    await deleteMessagesPerRoomId(room._id);
    await deleteRoom(room);
    tryGetRoomsController(ios);
  } catch (e) {
    throw e;
  }
};

const joinRoomController = async ({ roomId, socket }) => {
  try {
    socket.join(`/${roomId}`);
    const messages = await findMessagesPerRoomId(roomId);
    socket.emit("history", messages);
  } catch (e) {
    throw e;
  }
};

module.exports = {
  tryGetRoomsController,
  joinRoomController,
  createRoomController,
  updateRoomController,
  deleteRoomController,
};
