const mongoose = require("mongoose");
const schema = mongoose.Schema;

const roomSchema = schema({
  index: { type: Number, required: true },
  title: { type: String, required: true },
});

const Room = mongoose.model("room", roomSchema);

module.exports = Room;
