module.exports = {
  production: {
    certUrl: __dirname + '/ssl/production/',
    keyUrl: __dirname + '/ssl/production/',
    dbUrl: '',
    portHTTP: 80,
    portHTTPS: 443,
  },
  development: {
    certUrl: __dirname + '/ssl/development/localhost.crt',
    keyUrl: __dirname + '/ssl/development/localhost.key',
    dbUrl:
      'mongodb+srv://jean:123@cluster0-urpjt.gcp.mongodb.net/nodechapitre33?retryWrites=true',
    portHTTP: 3000,
    portHTTPS: 3001,
  },
};
