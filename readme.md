
## Nous lançons en essai un nouveau système de projet en partant du projet de chat de Node.js 👩‍💻👨‍💻 

> L’objectif est vous participiez à l'élaboration de l’équivalent du Discord. Tous les participants peuvent ainsi travailler en équipe sur un vrai projet ambitieux.

>>>
Les prérequis pour participer au projet sont : 
- HTML/CSS, 
- JavaScript, 
- Git
- Node.js
>>>

**Regardez la vidéo du chapitre de mise en place du projet dans le cours Node.js**

Le processus sera le suivant :

> 1. Nous créons quelques missions à effectuer dans une milestone et des issues exactement comme le ferait un chef de projet. 
> 
> 2. Vous devez proposer votre implémentation d’une ou plusieurs issues dans une pull request, et discuter des pull requests des autres personnes pour les améliorer.
>
> 3. Une fois le temps écoulé nous allons faire une implémentation de toutes les issues dans un chapitre avec les nouvelles fonctionnalités. Vous aurez ainsi un retour et éventuellement une autre manière de faire par rapport à votre pull request.

{+ Ce que ce projet va vous apporter : devenir autonome sur un projet réel, maîtriser un environnement professionnel, se confronter à des problèmes de plus en plus complexes, apprendre à collaborer en recevant des avis lors de reviews d’autres personnes. +}

[- Ce que ce projet n’est pas : vous n’aurez pas de correction personnalisée de chaque pull request, c’est complètement impossible et inmaintenable. Même si votre solution n’est pas parfaite et comporte des erreurs, ce n’est pas important car vous aurez une solution aux problèmes posés. L'important est de participer pour progresser ! -]

*Si cela fonctionne ce processus sera poursuivi sur Node.js avec plein de missions de plus en plus avancées et sera généralisé sur les autres formations.*

*Pour bénéficier au mieux de ce système, vous pouvez vous appeler sur Discord et discuter sur le chan dédié au projet sur Discord.* 

*Ceux ne participant pas auront quand même le bénéfice de voir un projet réel évoluer dans les vidéos, mais nous vous invitons à participer car ce système est conçu pour être proche d’un environnement de travail réel et vous fera beaucoup progresser (utilisation de Gitlab, de Git, review de code, discussions techniques, pull requests etc).*

*Si la participation n’est pas suffisante nous abandonnerons ce projet de test. C’est bien sûr un projet en plus des chapitres normaux.*
